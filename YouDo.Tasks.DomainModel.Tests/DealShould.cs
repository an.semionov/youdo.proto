﻿//using System;
//using Xunit;
//using YouDo.Tasks.DomainModel.ValueObjects;

//namespace YouDo.Tasks.DomainModel.Tests
//{
//    public class DealShould
//    {
//        [Fact]
//        public void BeCalculationExpireDateAsCreateDateAddMaxAge()
//        {
//            var maxAge = TimeSpan.FromSeconds(1);

//            var deal = new Deal(
//                new Task()
//                , Money.Rub(1)
//                , maxAge
//                , "description");

//            var estimatedDate = deal.CreateDate.AddSeconds(maxAge.Seconds);

//            Assert.Equal(estimatedDate, deal.ExpireDate, TimeSpan.FromMilliseconds(0));
//        }

//        [Fact]
//        public void BeBeCreatedWithStatusCreated()
//        {
//            var deal = new Deal(
//                new Task()
//                , Money.Rub(1)
//                , TimeSpan.FromSeconds(1)
//                , "description");

//            Assert.Equal(DealStatus.Created, deal.Status);
//        }

//        [Fact]
//        public void TransferToStatusPaymentProcessingIfCurrentStatusCreated()
//        {
//            var deal = new Deal(
//                new Task()
//                , Money.Rub(1)
//                , TimeSpan.FromSeconds(1)
//                , "description");

//            deal.SetStatusPaymentProcessing();

//            Assert.Equal(DealStatus.PaymentProcessing, deal.Status);
//        }

//        [Fact]
//        public void TransferToStatusPaidIfCurrentStatusPaymentProcessing()
//        {
//            var deal = new Deal(
//                new Task()
//                , Money.Rub(1)
//                , TimeSpan.FromSeconds(1)
//                , "description");
//            deal.SetStatusPaymentProcessing();

//            deal.SetStatusPaid();

//            Assert.Equal(DealStatus.Paid, deal.Status);
//        }

//        [Fact]
//        public void TransferToStatusPaymentProcessErrorIfCurrentStatusPaymentProcessing()
//        {
//            var deal = new Deal(
//                new Task()
//                , Money.Rub(1)
//                , TimeSpan.FromSeconds(1)
//                , "description");
//            deal.SetStatusPaymentProcessing();

//            deal.SetStatusPaymentProcessError();

//            Assert.Equal(DealStatus.PaymentProcessError, deal.Status);
//        }
//    }
//}
