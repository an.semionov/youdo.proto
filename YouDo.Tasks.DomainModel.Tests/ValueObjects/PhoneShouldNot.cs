﻿using System;
using Xunit;
using YouDo.Tasks.DomainModel.ValueObjects;

namespace YouDo.Tasks.DomainModel.Tests.ValueObjects
{
    public class PhoneShouldNot
    {
        [Fact]
        public void BeCreateWithIfPassNullPhone()
        {
            string phone = null;

            var ex = Assert.Throws<ArgumentException>(() => new Phone(phone));

            Assert.Equal("phone", ex.ParamName);
            Assert.Contains("Phone must not be null or empty", ex.Message);
        }

        [Fact]
        public void BeCreateWithIfPassEmptyPhone()
        {
            string phone = null;

            var ex = Assert.Throws<ArgumentException>(() => new Phone(phone));

            Assert.Equal("phone", ex.ParamName);
            Assert.Contains("Phone must not be null or empty", ex.Message);
        }
    }
}
