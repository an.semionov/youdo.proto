﻿using Xunit;
using YouDo.Tasks.DomainModel.ValueObjects;

namespace YouDo.Tasks.DomainModel.Tests.ValueObjects
{
    public class PhoneShould
    {
        [Fact]
        public void BeCreatedWithStatusNotСonfirmed()
        {
            var phone = new Phone(7, 459, 7896523);

            Assert.False(phone.IsConfirmed());
        }

        [Fact]
        public void ChangeStatusToСonfirmed()
        {
            var phone = new Phone(7, 459, 7896523);

            phone.Confirm();

            Assert.True(phone.IsConfirmed());
        }

        [Fact]
        public void BeCreatedIfPassPhone()
        {
            var phone = new Phone("79167648236");

            Assert.Equal(7, phone.CountryCode);
            Assert.Equal(916, phone.Prefix);
            Assert.Equal(7648236, phone.LineNumber);
        }
    }
}
