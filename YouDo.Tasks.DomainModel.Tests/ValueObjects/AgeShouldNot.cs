﻿using System;
using Xunit;
using YouDo.Tasks.DomainModel.ValueObjects;

namespace YouDo.Tasks.DomainModel.Tests.ValueObjects
{
    public class AgeShouldNot
    {
        [Fact]
        public void BeCreatedIfPassValueLessThan18()
        {
            var value = 17;

            var ex = Assert.Throws<ArgumentException>(() => new Age(value));

            Assert.Equal("value", ex.ParamName);
            Assert.Contains("Value must be greater than 17", ex.Message);
        }

        [Fact]
        public void BeCreatedIfPassValueGreaterThan99()
        {
            var value = 100;

            var ex = Assert.Throws<ArgumentException>(() => new Age(value));

            Assert.Equal("value", ex.ParamName);
            Assert.Contains("Value must be less than 100", ex.Message);
        }
    }
}
