﻿using System;
using Xunit;
using YouDo.Tasks.DomainModel.ValueObjects;
using YouDo.Tasks.DomainModel.ValueObjects.TaskDescriptions;

namespace YouDo.Tasks.DomainModel.Tests.ValueObjects.TaskDescriptions
{
    public class TaskDescriptionShouldNot
    {
        [Fact]
        public void BeCreatedIfPassNullPrice()
        {
            Money price = null;

            var ex = Assert.Throws<ArgumentNullException>(() => new TaskDescriptionTest(
                 "description"
                , price));

            Assert.Equal("price", ex.ParamName);
            Assert.Contains("price", ex.Message);
        }


        [Fact]
        public void BeCreatedIfPassNullDescription()
        {
            var price = Money.Rub(15);
            string description = null; 

            var ex = Assert.Throws<ArgumentException>(() => new TaskDescriptionTest(
                 description
                , price));

            Assert.Equal("description", ex.ParamName);
            Assert.Contains("Description must not be null or empty", ex.Message);
        }

        [Fact]
        public void BeCreatedIfPassEmptyDescription()
        {
            var price = Money.Rub(15);
            var description = "";

            var ex = Assert.Throws<ArgumentException>(() => new TaskDescriptionTest(
                 description
                , price));

            Assert.Equal("description", ex.ParamName);
            Assert.Contains("Description must not be null or empty", ex.Message);
        }
    }

    public class TaskDescriptionTest : TaskDescription
    {
        public TaskDescriptionTest(string description, Money price) : base(description, price)
        {
        }
    }
}
