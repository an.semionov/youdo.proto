﻿using System;
using Xunit;
using YouDo.Tasks.DomainModel.ValueObjects;

namespace YouDo.Tasks.DomainModel.Tests.ValueObjects
{
    public class FullNameShould
    {
        [Fact]
        public void BeCreateWithNullSurname()
        {
            string surname = null;

            var ex = Assert.Throws<ArgumentException>(() => new FullName(
                surname
                , "Name"));

            Assert.Equal("surname", ex.ParamName);
            Assert.Contains("Surname must not be null or empty", ex.Message);
        }

        [Fact]
        public void BeCreateWithEmptySurname()
        {
            string surname = string.Empty;

            var ex = Assert.Throws<ArgumentException>(() => new FullName(
                surname
                , "Name"));

            Assert.Equal("surname", ex.ParamName);
            Assert.Contains("Surname must not be null or empty", ex.Message);
        }

        [Fact]
        public void BeCreateWithNullName()
        {
            string name = null;

            var ex = Assert.Throws<ArgumentException>(() => new FullName(
                "surname"
                , name));

            Assert.Equal("name", ex.ParamName);
            Assert.Contains("Name must not be null or empty", ex.Message);
        }

        [Fact]
        public void BeCreateWithEmptyName()
        {
            string name = string.Empty;

            var ex = Assert.Throws<ArgumentException>(() => new FullName(
                "surname"
                , name));

            Assert.Equal("name", ex.ParamName);
            Assert.Contains("Name must not be null or empty", ex.Message);
        }

    }
}
