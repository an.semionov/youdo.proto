//using System;
//using Xunit;
//using YouDo.Tasks.DomainModel.ValueObjects;

//namespace YouDo.Tasks.DomainModel.Tests
//{
//    public class DealShouldNot
//    {
//        [Fact]
//        public void BeCreatedIfPassNullTask()
//        {
//            Task task = null;

//            var ex = Assert.Throws<ArgumentNullException>(() => new Deal(
//                task
//                , Money.Rub(1)
//                , TimeSpan.FromSeconds(1)
//                , "description"));

//            Assert.Equal("task", ex.ParamName);
//        }

//        [Fact]
//        public void BeCreatedIfPassNullAmount()
//        {
//            Money amount = null;

//            var ex = Assert.Throws<ArgumentNullException>(() => new Deal(
//                new Task()
//                , amount
//                , TimeSpan.FromSeconds(1)
//                , "description"));

//            Assert.Equal("amount", ex.ParamName);
//        }

//        [Fact]
//        public void BeCreatedIfPassAmountLessThanOne()
//        {
//            var amount = Money.Rub(0);

//            var ex = Assert.Throws<ArgumentException>(() => new Deal(
//                new Task()
//                , amount
//                , TimeSpan.FromSeconds(1)
//                , "description"));

//            Assert.Equal("amount", ex.ParamName);
//            Assert.Contains("Amount must be greater than zero", ex.Message);
//        }
        
//        [Fact]
//        public void BeCreatedIfPassMaxAgeLessThanOneSecond()
//        {
//            var maxAge = TimeSpan.FromSeconds(0);

//            var ex = Assert.Throws<ArgumentException>(() => new Deal(
//                new Task()
//                , Money.Rub(1)
//                , maxAge
//                , "description"));

//            Assert.Equal("maxAge", ex.ParamName);
//            Assert.Contains("MaxAge must be greater than zero second", ex.Message);
//        }

//        [Fact]
//        public void BeCreatedIfPassNullDescription()
//        {
//            string description = null;

//            var ex = Assert.Throws<ArgumentException>(() => new Deal(
//                new Task()
//                , Money.Rub(1)
//                , TimeSpan.FromSeconds(1)
//                , description));

//            Assert.Equal("description", ex.ParamName);
//            Assert.Contains("Description must not be null or empty", ex.Message);
//        }

//        [Fact]
//        public void BeCreatedIfPassEmptyDescription()
//        {
//            string description = string.Empty;

//            var ex = Assert.Throws<ArgumentException>(() => new Deal(
//                new Task()
//                , Money.Rub(1)
//                , TimeSpan.FromSeconds(1)
//                , description));

//            Assert.Equal("description", ex.ParamName);
//            Assert.Contains("Description must not be null or empty", ex.Message);
//        }

//        [Fact]
//        public void BeCreatedIfPassDescriptionContainsOnlyWhiteSpace()
//        {
//            var description = " ";

//            var ex = Assert.Throws<ArgumentException>(() => new Deal(
//                new Task()
//                , Money.Rub(1)
//                , TimeSpan.FromSeconds(1)
//                , description));

//            Assert.Equal("description", ex.ParamName);
//            Assert.Contains("Description must not be null or empty", ex.Message);
//        }

//        [Fact]
//        public void TransferToStatusPaymentProcessingIfCurrentStatusNotCreated()
//        {
//            var deal = new Deal(
//                new Task()
//                , Money.Rub(1)
//                , TimeSpan.FromSeconds(1)
//                , "description");

//            deal.SetStatusPaymentProcessing();

//            var ex = Assert.Throws<InvalidOperationException>(() => deal.SetStatusPaymentProcessing());
            
//            Assert.Equal("Deal status must be Created", ex.Message);
//        }

//        [Fact]
//        public void TransferToStatusPaidIfCurrentStatusNotPaymentProcessing()
//        {
//            var deal = new Deal(
//                new Task()
//                , Money.Rub(1)
//                , TimeSpan.FromSeconds(1)
//                , "description");

//            var ex = Assert.Throws<InvalidOperationException>(() => deal.SetStatusPaid());

//            Assert.Equal("Deal status must be PaymentProcessing", ex.Message);
//        }

//        [Fact]
//        public void TransferToStatusPaymentProcessErrorIfCurrentStatusNotPaymentProcessing()
//        {
//            var deal = new Deal(
//                new Task()
//                , Money.Rub(1)
//                , TimeSpan.FromSeconds(1)
//                , "description");

//            var ex = Assert.Throws<InvalidOperationException>(() => deal.SetStatusPaymentProcessError());

//            Assert.Equal("Deal status must be PaymentProcessing", ex.Message);
//        }
//    }
//}
