﻿using Xunit;
using YouDo.Tasks.DomainModel.Entities;
using YouDo.Tasks.DomainModel.ValueObjects;

namespace YouDo.Tasks.DomainModel.Tests.Entities
{
    public class TaskOwnerShould
    {
        [Fact]
        public void BeCreateAsValidIfPassСonfirmedPhone()
        {
            FullName fullName = new FullName("Surname", "Name");
            var phone = new Phone(7, 495, 7852536);
            phone.Confirm();

            var taskOwner = new TaskOwner(
                fullName,
                phone);

            Assert.True(taskOwner.IsValid());
        }

        [Fact]
        public void BeCreateAsNotValidIfPassNotСonfirmedPhone()
        {
            FullName fullName = new FullName("Surname", "Name");
            var phone = new Phone(7, 495, 7852536);

            var taskOwner = new TaskOwner(
                fullName,
                phone);

            Assert.False(taskOwner.IsValid());
        }

        [Fact]
        public void ChangedToNotValidIfSetNotСonfirmedPhone()
        {
            FullName fullName = new FullName("Surname", "Name");
            var phone = new Phone(7, 495, 7852536);
            
            var taskOwner = new TaskOwner(
                fullName,
                phone);

            taskOwner.SetPhone(new Phone(7, 499, 5632536));

            Assert.False(taskOwner.IsValid());
        }
    }
}
