﻿using System;
using Xunit;
using YouDo.Tasks.DomainModel.Entities;
using YouDo.Tasks.DomainModel.ValueObjects;

namespace YouDo.Tasks.DomainModel.Tests.Entities
{
    public class TaskOwnerShouldNot
    {
        [Fact]
        public void BeCreateWithNullFullName()
        {
            FullName fullName = null;
            var phone = new Phone(7, 495, 7852536);

            var ex = Assert.Throws<ArgumentNullException>(() => new TaskOwner(
                fullName, 
                phone));

            Assert.Equal("fullName", ex.ParamName);
        }

        [Fact]
        public void BeCreateWithNullPhone()
        {
            var fullName = new FullName("surname", "Name");
            Phone phone = null;

            var ex = Assert.Throws<ArgumentNullException>(() => new TaskOwner(
                fullName
                , phone));

            Assert.Equal("phone", ex.ParamName);
        }

        [Fact]
        public void SetNullPhone()
        {
            var fullName = new FullName("surname", "Name");
            Phone phone = new Phone(7, 495, 7852536);

            var taskOwner = new TaskOwner(
                fullName
                , phone);

            var ex = Assert.Throws<ArgumentNullException>(() => taskOwner.SetPhone(null));

            Assert.Equal("phone", ex.ParamName);
        }
    }
}
