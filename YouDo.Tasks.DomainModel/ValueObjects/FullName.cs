﻿using System;

namespace YouDo.Tasks.DomainModel.ValueObjects
{
    public class FullName : ValueObject
    {
        public string Surname { get; private set; }
        public string Name { get; private set; }

        public FullName(string surname, string name)
        {
            if (string.IsNullOrEmpty(surname) || string.IsNullOrWhiteSpace(surname))
            {
                throw new ArgumentException("Surname must not be null or empty", nameof(surname));
            }

            if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Name must not be null or empty", nameof(name));
            }

            Surname = surname;
            Name = name;
        }
    }
}
