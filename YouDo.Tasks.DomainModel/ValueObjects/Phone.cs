﻿using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("YouDo.Tasks.DomainModel.Tests")]
namespace YouDo.Tasks.DomainModel.ValueObjects
{
    public class Phone : ValueObject
    {
        public int CountryCode { get; private set; }
        public int Prefix { get; private set; }
        public int LineNumber { get; private set; }
        public PhoneStatus Status { get; private set; } = PhoneStatus.NotConfirmed;

        public Phone(int countryCode, int prefix, int lineNumber)
        {
            CountryCode = countryCode;
            Prefix = prefix;
            LineNumber = lineNumber;
        }

        public Phone(string phone)
        {
            if (string.IsNullOrEmpty(phone) || string.IsNullOrWhiteSpace(phone))
            {
                throw new ArgumentException("Phone must not be null or empty", nameof(phone));
            }

            CountryCode = int.Parse(phone.Substring(0, 1));
            Prefix = int.Parse(phone.Substring(1, 3));
            LineNumber = int.Parse(phone.Substring(4));
        }

        internal void Confirm()
        {
            Status = PhoneStatus.Confirmed;
        }

        public bool IsConfirmed()
        {
            return Status == PhoneStatus.Confirmed;
        }
    }

    public enum PhoneStatus
    {
        Confirmed,
        NotConfirmed
    }
}
