﻿using System;

namespace YouDo.Tasks.DomainModel.ValueObjects.TaskDescriptions
{
    public class TaskDescription: ValueObject
    {
        public string Description { get; }
        public Money Price { get; }

        protected TaskDescription(string description, Money price)
        {
            if (string.IsNullOrEmpty(description) || string.IsNullOrWhiteSpace(description))
            {
                throw new ArgumentException("Description must not be null or empty", nameof(description));
            }
            price = price ?? throw new ArgumentNullException(nameof(price));
            if (price.Value <= 0)
            {
                throw new ArgumentException("Price must be greater than zero", nameof(price));
            }

            Description = description;
            Price = price;
        }
    }
}
