﻿
namespace YouDo.Tasks.DomainModel.ValueObjects.TaskDescriptions
{
    public class PhonesAndTabletsTaskDescription : TaskDescription
    {
        public PhonesAndTabletsTaskDescription(string description, Money price) : base(description, price)
        {
        }
    }
}
