﻿using System;

namespace YouDo.Tasks.DomainModel.ValueObjects
{
    public class Age: ValueObject
    {
        public int Value { get; private set; }

        public Age(int value)
        {
            if (value < 18)
            {
                throw new ArgumentException("Value must be greater than 17", nameof(value));
            }

            if (value > 99)
            {
                throw new ArgumentException("Value must be less than 100", nameof(value));
            }

            Value = value;
        }
    }
}
