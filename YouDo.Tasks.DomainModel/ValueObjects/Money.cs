﻿namespace YouDo.Tasks.DomainModel.ValueObjects
{
    public class Money : ValueObject
    {
        public decimal Value { get; private set; }
        public Currency Currency { get; private set; }

        public Money(decimal value, Currency currency)
        {
            Value = value;
            Currency = currency;
        }

        public static Money Rub(decimal amount)
        {
            return new Money(amount, Currency.Rub);
        }
    }

    public enum Currency
    {
        Rub	= 643
    }
}
