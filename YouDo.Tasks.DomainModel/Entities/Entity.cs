﻿using MediatR;
using System.Collections.Generic;

namespace YouDo.Tasks.DomainModel.Entities
{
    public class Entity
    {
        public int Id { get; internal set; }

        private List<INotification> _domainEvents = new List<INotification>();
        public IReadOnlyCollection<INotification> DomainEvents => _domainEvents?.AsReadOnly();

        public void AddDomainEvent(INotification eventItem)
        {
            _domainEvents.Add(eventItem);
        }
    }
}
