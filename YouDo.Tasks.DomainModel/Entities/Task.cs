﻿using System;
using YouDo.Tasks.DomainModel.Events;
using YouDo.Tasks.DomainModel.ValueObjects.TaskDescriptions;

namespace YouDo.Tasks.DomainModel.Entities
{
    public class Task : Entity
    {
        public int OwnerId { get; private set; }
        public int CategoryId { get; private set; }
        public TaskDescription Description { get; private set; }

        internal Task(TaskOwner taskOwner, int categoryId, TaskDescription description)
        {
            description = description ?? throw new ArgumentNullException(nameof(description));
            taskOwner = taskOwner ?? throw new ArgumentNullException(nameof(taskOwner));

            OwnerId = taskOwner.Id;
            CategoryId = categoryId;
            Description = description;

            AddDomainEvent(new TaskCreatedDomainEvent(this));
        }
    }
}
