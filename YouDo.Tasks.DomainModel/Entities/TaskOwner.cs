﻿using System;
using YouDo.Tasks.DomainModel.Events;
using YouDo.Tasks.DomainModel.ValueObjects;

namespace YouDo.Tasks.DomainModel.Entities
{
    public class TaskOwner : Entity
    {
        private FullName FullName { get; set; }
        private Phone Phone { get; set; }
        private TaskOwnerStatus Status { get; set; } = TaskOwnerStatus.NotValid;

        public TaskOwner(FullName fullName, Phone phone)
        {
            FullName = fullName ?? throw new ArgumentNullException(nameof(fullName));
            Phone = phone ?? throw new ArgumentNullException(nameof(phone));

            AddDomainEvent(new TaskOwnerCreatedDomainEvent(this));

            Validate();
        }

        public void SetPhone(Phone phone)
        {
            Phone = phone ?? throw new ArgumentNullException(nameof(phone));
            Validate();
        }

        private void Validate()
        {
            if (!Phone.IsConfirmed())
            {
                Status = TaskOwnerStatus.NotValid;
                AddDomainEvent(new TaskOwnerChangedToNotValidDomainEvent(this));

                return;
            }

            Status = TaskOwnerStatus.Valid;
            AddDomainEvent(new TaskOwnerChangedToValidDomainEvent(this));
        }

        public bool IsValid()
        {
            return Status == TaskOwnerStatus.Valid;
        }
    }

    public enum TaskOwnerStatus
    {
        Valid,
        NotValid,
    }
}
