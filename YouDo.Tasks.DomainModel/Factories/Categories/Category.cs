﻿using System;
using YouDo.Tasks.DomainModel.Entities;
using YouDo.Tasks.DomainModel.ValueObjects.TaskDescriptions;

namespace YouDo.Tasks.DomainModel.Factories.Categories
{
    public abstract class Category
    {
        protected abstract int Categoryid { get; }

        public Task Create(TaskOwner taskOwner, PhonesAndTabletsTaskDescription description)
        {
            Valid(taskOwner);
            return new Task(taskOwner, Categoryid, description);
        }

        //Кидать исключение если нельзя пользователю создавать задачи в конкретной категории
        protected abstract void Valid(TaskOwner taskOwner);

        public static Category Create(int categoryid)
        {
            switch (categoryid)
            {
                case 1:
                    return new PhonesAndTabletsCategory();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}