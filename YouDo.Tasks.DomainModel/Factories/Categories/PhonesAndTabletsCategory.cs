﻿using YouDo.Tasks.DomainModel.Entities;
using YouDo.Tasks.DomainModel.ValueObjects.TaskDescriptions;

namespace YouDo.Tasks.DomainModel.Factories.Categories
{
    public class PhonesAndTabletsCategory : Category
    {
        protected override int Categoryid => 1;

        public Task Create(TaskOwner taskOwner, PhonesAndTabletsTaskDescription description)
        {
            Valid(taskOwner);

            return new Task(taskOwner, 15, description);
        }

        protected override void Valid(TaskOwner taskOwner)
        {
            //Кидать исключение если нельзя пользователю создавать задачи в конкретной категории
        }
    }
}
