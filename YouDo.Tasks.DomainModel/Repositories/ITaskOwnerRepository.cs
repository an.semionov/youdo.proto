﻿using System.Threading.Tasks;
using YouDo.Tasks.DomainModel.Entities;
using Threading = System.Threading.Tasks;

namespace YouDo.Tasks.DomainModel.Repositories
{
    public interface ITaskOwnerRepository
    {
        Task<TaskOwner> GetAsync(int id);
        Threading.Task SaveAsync(TaskOwner taskOwner);
    }
}
