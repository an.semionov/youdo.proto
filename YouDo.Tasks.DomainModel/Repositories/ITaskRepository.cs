﻿using Threading = System.Threading.Tasks;

namespace YouDo.Tasks.DomainModel.Repositories
{
    public interface ITaskRepository
    {
        Threading.Task<Entities.Task> GetAsync(int id);
        Threading.Task SaveAsync(Entities.Task taskOwner);
    }
}
