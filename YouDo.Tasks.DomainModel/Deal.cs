﻿using System;
using System.ComponentModel;
using YouDo.Tasks.DomainModel.Entities;
using YouDo.Tasks.DomainModel.ValueObjects;

namespace YouDo.Tasks.DomainModel
{
    public class Deal : Entity
    {
        public int TaskId { get; private set; }
        public Money Amount { get; private set; }
        public string Description { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime ExpireDate { get; private set; }
        public DealStatus Status { get; private set; }
        
        public Deal(Task task, Money amount, TimeSpan maxAge, string description)
        {
            task = task ?? throw new ArgumentNullException(nameof(task));
            Amount = amount ?? throw new ArgumentNullException(nameof(amount));
            if (Amount.Value <= 0)
            {
                throw new ArgumentException("Amount must be greater than zero", nameof(amount));
            }
            if (maxAge.Seconds <= 0)
            {
                throw new ArgumentException("MaxAge must be greater than zero second", nameof(maxAge));
            }
            if (string.IsNullOrEmpty(description) || string.IsNullOrWhiteSpace(description))
            {
                throw new ArgumentException("Description must not be null or empty", nameof(description));
            }

            TaskId = task.Id;
            CreateDate = DateTime.UtcNow;
            ExpireDate = CalculationExpireDate(CreateDate, maxAge);
            Status = DealStatus.Created;
        }

        public void SetStatusPaymentProcessing()
        {
            if (Status != DealStatus.Created)
            {
                throw new InvalidOperationException("Deal status must be Created");
            }

            Status = DealStatus.PaymentProcessing;
        }

        public void SetStatusPaid()
        {
            if (Status != DealStatus.PaymentProcessing)
            {
                throw new InvalidOperationException("Deal status must be PaymentProcessing");
            }

            Status = DealStatus.Paid;
        }

        public void SetStatusPaymentProcessError()
        {
            if (Status != DealStatus.PaymentProcessing)
            {
                throw new InvalidOperationException("Deal status must be PaymentProcessing");
            }

            Status = DealStatus.PaymentProcessError;
        }

        private DateTime CalculationExpireDate(DateTime createDate, TimeSpan age)
        {
            return createDate.AddSeconds(age.Seconds);
        }
    }

    public enum DealStatus
    {
        /// <summary>
        /// Сделка зарегистрирована в системе
        /// </summary>
        [Description("Сделка зарегистрирована в W1, но еще не оплачена")]
        Created = 0,

        /// <summary>
        /// Производится оплата сделки заказчиком
        /// </summary>
        [Description("Производится резервирование оплаты сделки заказчиком")]
        PaymentProcessing = 1,

        /// <summary>
        /// Ошибка в процессе оплаты сделки
        /// </summary>
        [Description("Ошибка в процессе резервирования оплаты заказчиком")]
        PaymentProcessError = 2,

        /// <summary>
        /// Сделка успешно оплачена заказчиком
        /// </summary>
        [Description("Резервирование оплаты сделки заказчиком прошло успешно")]
        Paid = 3,

        /// <summary>
        /// Производится выплата исполнителю
        /// </summary>
        [Description("Производится выплата исполнителю")]
        PayoutProcessing = 4,

        /// <summary>
        /// Ошибка в процессе выплаты исполнителю
        /// </summary>
        [Description("Ошибка в процессе выплаты исполнителю")]
        PayoutProcessError = 5,

        /// <summary>
        /// Сумма сделки успешно выплачена исполнителю
        /// </summary>
        [Description("Сумма сделки успешно выплачена исполнителю")]
        Completed = 6,

        /// <summary>
        /// Сделка отменяется (возврат заказчику)
        /// </summary>
        [Description("Производится возврат денег заказчику")]
        Canceling = 7,

        /// <summary>
        /// Ошибка в процессе отмены сделки
        /// </summary>
        [Description("Ошибка в процессе возврата денег заказчику")]
        CancelError = 8,

        /// <summary>
        /// Сделка успешно отменена
        /// </summary>
        [Description("Деньги возвращены заказчику")]
        Canceled = 9,

        /// <summary>
        /// Средства захолдированы на карте
        /// </summary>
        [Description("Средства захолдированы на карте")]
        PaymentHold = 10,

        /// <summary>
        /// Сделка в процессе завершения оплаты либо отмены
        /// </summary>
        [Description("Сделка в процессе завершения оплаты либо отмены")]
        PaymentHoldProcessing = 11,

        /// <summary>
        /// Сделка перешла в архив
        /// </summary>
        [Description("Сделка перешла в архив по таймауту")]
        Archived = 12
    }
}
