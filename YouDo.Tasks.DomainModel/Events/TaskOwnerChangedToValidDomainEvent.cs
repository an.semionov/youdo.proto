﻿using MediatR;
using YouDo.Tasks.DomainModel.Entities;

namespace YouDo.Tasks.DomainModel.Events
{
    public class TaskOwnerChangedToValidDomainEvent
        : INotification
    {
        public TaskOwner TaskOwner { get; }

        public TaskOwnerChangedToValidDomainEvent(TaskOwner taskOwner)
        {
            TaskOwner = taskOwner;
        }
    }
}
