﻿using MediatR;
using YouDo.Tasks.DomainModel.Entities;

namespace YouDo.Tasks.DomainModel.Events
{
    public class TaskOwnerCreatedDomainEvent 
        : INotification
    {
        public TaskOwner TaskOwner { get; }

        public TaskOwnerCreatedDomainEvent(TaskOwner taskOwner)
        {
            TaskOwner = taskOwner;
        }
    }
}
