﻿using MediatR;
using YouDo.Tasks.DomainModel.Entities;

namespace YouDo.Tasks.DomainModel.Events
{
    public class TaskOwnerChangedToNotValidDomainEvent
        : INotification
    {
        public TaskOwner TaskOwner { get; }

        public TaskOwnerChangedToNotValidDomainEvent(TaskOwner taskOwner)
        {
            TaskOwner = taskOwner;
        }
    }
}
