﻿using MediatR;
using YouDo.Tasks.DomainModel.Entities;

namespace YouDo.Tasks.DomainModel.Events
{
    public class TaskCreatedDomainEvent
        : INotification
    {
        public Task Task { get; }

        public TaskCreatedDomainEvent(Task task)
        {
            Task = task ;
        }
    }
}
