﻿using Threading = System.Threading.Tasks;
using YouDo.Tasks.DomainModel.Entities;
using YouDo.Tasks.DomainModel.Repositories;

namespace YouDo.Tasks.App.Repositories
{
    class TaskOwnerRepository : ITaskOwnerRepository
    {
        public async Threading.Task<TaskOwner> GetAsync(int id)
        {
            return await Threading.Task.FromResult<TaskOwner>(null);
        }

        public async Threading.Task SaveAsync(TaskOwner taskOwner)
        {
            
        }
    }
}
