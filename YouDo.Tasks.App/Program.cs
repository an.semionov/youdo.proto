﻿using System;
using YouDo.Tasks.App.Repositories;
using YouDo.Tasks.App.Services;
using YouDo.Tasks.DomainModel.Entities;
using YouDo.Tasks.DomainModel.Factories.Categories;
using YouDo.Tasks.DomainModel.Repositories;
using YouDo.Tasks.DomainModel.ValueObjects;
using YouDo.Tasks.DomainModel.ValueObjects.TaskDescriptions;
using Threading = System.Threading.Tasks;

namespace YouDo.Tasks.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var request = new Request
            {
                CategoryId = 1,
                Price = 5000,
                Description = "Description",
            };

            var controller = new Controller();

            controller.CurrentUserAddNewTask(request).Wait();
        }
    }

    class Controller
    {
        private static readonly int CurrentUserId = 1;

        private ITaskOwnerRepository _taskOwnerRepository = new TaskOwnerRepository();
        private ITaskRepository _taskRepository = new TaskRepository();
        private UserService _userService = new UserService();

        public async Threading.Task CurrentUserAddNewTask(Request request)
        {
            var taskOwner = await _taskOwnerRepository.GetAsync(CurrentUserId);

            if (taskOwner == null)
            {
                //получить пользователя из внешнего контекста
                var user = _userService.GetUser(CurrentUserId) ?? throw new InvalidOperationException();

                var fullName = new FullName(user.Surname, user.Name);
                var phone = new Phone(user.Phone);
                taskOwner = new TaskOwner(fullName, phone);

                await _taskOwnerRepository.SaveAsync(taskOwner);
            }

            var category = Category.Create(request.CategoryId);

            var description = new PhonesAndTabletsTaskDescription(request.Description, Money.Rub(request.Price));
            var task = category.Create(taskOwner, description);

            await _taskRepository.SaveAsync(task);
        }
    }

    public class Request
    {
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int CategoryId { get; set; }
    }

}
