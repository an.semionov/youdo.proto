﻿
namespace YouDo.Tasks.App.Model
{
    public class User
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
    }
}
