﻿using System.Threading.Tasks;

namespace YouDo.Tasks.Services
{
    public class PaymentService : IPaymentService
    {
        public Task HoldMoney(int dealId)
        {
            throw new System.NotImplementedException();
        }
    }

    public interface IPaymentService
    {
        Task HoldMoney(int dealId);
    }
}
