﻿using System;
using Automatonymous;

namespace YouDo.Tasks.Saga.Model
{
    public class DealMachineState : SagaStateMachineInstance
    {
        public Guid CorrelationId { get; set; }
        public string State { get; set; }
        public int DealId { get; set; }
    }
}
