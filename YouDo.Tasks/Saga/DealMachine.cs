﻿//using Automatonymous;
//using System;
//using YouDo.Tasks.DomainModel.Repositories;
//using YouDo.Tasks.Saga.Events;
//using YouDo.Tasks.Saga.Model;
//using YouDo.Tasks.Services;

//namespace YouDo.Tasks.Saga
//{
//    public class DealMachine : MassTransitStateMachine<DealMachineState>
//    {
//        public State PaymentProcessing { get; private set; }
//        public State PaymentError { get; private set; }
//        public State Paid { get; private set; }

//        public Event<DealCreated> Created { get; set; }
//        public Event PaymentProcessSuccess { get; set; }
//        public Event PaymentProcessFaild { get; set; }

//        private IPaymentService _paymentService;
//        private IDealRepository _dealRepository;

//        public DealMachine(
//            IPaymentService paymentService
//            , IDealRepository dealRepository)
//        {
//            _paymentService = paymentService
//                ?? throw new ArgumentNullException(nameof(paymentService));
//            _dealRepository = dealRepository
//                ?? throw new ArgumentNullException(nameof(dealRepository));

//            InstanceState(x => x.State);

//            Initially(
//                When(Created)
//                .Then(context => 
//                {
//                    var dealId = context.Instance.DealId;
//                    _paymentService.HoldMoney(dealId);
//                })
//                .TransitionTo(PaymentProcessing));

//            During(PaymentProcessing,
//                When(PaymentProcessSuccess)
//                .Then(context => {

//                })
//                .TransitionTo(Paid),
//                When(PaymentProcessFaild)
//                .Then(context => { /* do stuff */})
//                .TransitionTo(PaymentError)
//            );
//        }
//    }
//}
