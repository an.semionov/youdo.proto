﻿namespace YouDo.Tasks.Saga.Events
{
    public class DealCreated
    {
        public int DealId { get; set; }
    }
}
